//
//  ClassCard.swift
//  NikeApp
//
//  Created by Mac on 3/3/23.
//

import Foundation
import UIKit
class CardID:UIView{
    override init(frame:CGRect){
        super.init(frame:frame)
        SuperSetup()
    }
    required init?(coder:NSCoder){
        super.init(coder:coder)
        SuperSetup()
        
    }
    private func SuperSetup(){
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = .zero
        layer.shadowRadius = 10
        layer.shadowOpacity = 0.1
        layer.cornerRadius = 10
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 2
        
    }
}
