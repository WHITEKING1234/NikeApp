//
//  CollectionViewCell Info Nike.swift
//  NikeApp
//
//  Created by Mac on 14/3/23.
//

import UIKit

class CollectionViewCell_Info_Nike: UICollectionViewCell {
    
    @IBOutlet weak var imgaNike: UIImageView!
    
    static let shared = (String(describing: CollectionViewCell_Info_Nike.self))
}
