//
//  ViewController.swift
//  NikeApp
//
//  Created by Mac on 28/2/23.
//

import UIKit

class ViewController: UIViewController {
    
    
    
    @IBOutlet weak var CollectionView: UICollectionView!
    
    
    
    @IBOutlet weak var Coll2: UICollectionView!
    
    
    
    @IBOutlet weak var ButtonGetStarted: UIButton!
    
    @IBOutlet weak var NikeIcon: UIImageView!
    
    
    var NakeArray:[Nike] = [.init(name: "Air Jordan 1", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/011/119/994/original/218099_00.png.png", price: "$16000"),
                            .init(name: "Air Jordan 4 Retro OG GS", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/020/806/444/original/507844_00.png.png", price: "$14000"),
                            .init(name: "Air Jordan 1", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/011/119/994/original/218099_00.png.png", price: "$16000"),
                            .init(name: "Air Jordan 1", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/011/119/994/original/218099_00.png.png", price: "$16000"),
                            .init(name: "Air Jordan 11 Retro 'Win Like", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/008/870/353/original/235806_00.png.png", price: "$18000"),
                            //1
                            .init(name: "Wmns Air Jordan 1 Retro High OG 'Twist", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/021/545/481/original/509480_00.png.png", price: "$16000"),
                            .init(name: "Air Jordan 1 Mid 'Multicolor Swoosh Black'", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/012/910/690/original/554724_065.png.png", price: "$11000"),
                            .init(name: "Wmns Air Jordan 12 Retro 'Reptile", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/021/042/384/original/500924_00.png.png", price: "$19000"),
                            .init(name: "OFF-WHITE x Air Jordan 1", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/012/219/518/original/335047_00.png.png", price: "$12000"),
                            .init(name: "Trophy Room x Air Jordan 5", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/021/474/777/original/TR_JSP_5_ICE.png.png", price: "$30000"),
                            .init(name: "Travis Scott x Air Jordan 4", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/012/478/518/original/365514_00.png.png", price: "$10000000"),
                            .init(name: "Air Jordan 11 Retro Low ", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/021/219/116/original/CD6846_002.png.png", price: "$2000"),
                            .init(name: "Air Jordan 1 Retro High OG 'Bred Toe", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/009/559/594/original/144690_00.png.png", price: "$100"),
                            .init(name: "https://image.goat.com/375/attachments/product_template_pictures/images/020/270/533/original/CD6578_507.png.png'", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/020/270/533/original/CD6578_507.png.png", price: "$16943"),
                            .init(name: "Air Jordan 1 Retro High OG", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/018/552/901/original/404758_00.png.png", price: "$5000"),
                            .init(name: "Patta x Air Jordan 7", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/021/321/847/original/473391_00.png.png", price: "$3320"),
                            .init(name: "Travis Scott x Air Jordan 1", Image: "https://image.goat.com/375/attachments/product_template_pictures/images/018/783/111/original/488879_00.png.png", price: "$500032"),]
    
    
    @IBOutlet weak var UIimageKroos: UIImageView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        register()
        CollectionView.delegate = self
        CollectionView.dataSource = self
     
        NikeIcon.layer.shadowColor = UIColor.black.cgColor
        NikeIcon.layer.shadowOffset = .zero
        //
        NikeIcon.layer.shadowOpacity = 0.7        //Button
        //        ButtonGetStarted.layer.cornerRadius = 26
        //        ButtonGetStarted.clipsToBounds = true
        //        ButtonGetStarted.layer.shadowRadius = 15
        //        ButtonGetStarted.layer.shadowOffset = .zero
        //        ButtonGetStarted.layer.shadowOpacity = 0.5
        //        ButtonGetStarted.layer.shadowColor = UIColor.black.cgColor
        //        //Image
        //        UIimageKroos.layer.shadowRadius = 15
        //        UIimageKroos.layer.shadowOffset = .zero
        //        UIimageKroos.layer.shadowOpacity = 0.5
        //        UIimageKroos.layer.shadowColor = UIColor.black.cgColor
        //        UIimageKroos.layer.shadowPath = UIBezierPath(rect: UIimageKroos.bounds).cgPath
        // Do any additional setup after loading the view.
    }
    
    private func register(){
        CollectionView.register(UINib(nibName: CollectionViewCell.shared, bundle: nil),forCellWithReuseIdentifier: CollectionViewCell.shared)
        Coll2.register(UINib(nibName: CollectionViewCell.shared, bundle: nil),forCellWithReuseIdentifier: CollectionViewCell.shared)
       
    }
        
    
}
    
    
extension ViewController:UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return NakeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.shared, for: indexPath) as! CollectionViewCell
        cell.setup(Nikeapp: NakeArray[indexPath.row])
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NikeViewController") as! NikeViewController
        let model = NakeArray[indexPath.row]
        vc.dish = model
        navigationController?.pushViewController(vc, animated: true)
    }
}
    
    extension ViewController:UICollectionViewDelegateFlowLayout{
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            CGSize(width: 180, height: 190)
        }
    }



