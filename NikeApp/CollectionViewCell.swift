//
//  CollectionViewCell.swift
//  NikeApp
//
//  Created by Mac on 2/3/23.
//

import UIKit
import Kingfisher

class CollectionViewCell: UICollectionViewCell {
    
    
    static let shared = (String(describing: CollectionViewCell.self))
    
    
    @IBOutlet weak var NamaBrandLable: UILabel!
    
    @IBOutlet weak var PriceLable: UILabel!
  
    
    @IBOutlet weak var NikeImage: UIImageView!
    
    
    

    
    func setup(Nikeapp:Nike){
        NamaBrandLable.text = Nikeapp.name
        NikeImage.kf.setImage(with: URL(string: Nikeapp.Image))
        PriceLable.text = Nikeapp.price
        NikeImage.transform = CGAffineTransformMakeRotation(44.90)
        NikeImage.layer.shadowRadius = 0.4
        NikeImage.layer.shadowColor = UIColor.black.cgColor
        NikeImage.layer.shadowOffset = .zero
        NikeImage.layer.shadowOpacity = 1
    }
    
    
}

