//
//  TabbarVC.swift
//  NikeApp
//
//  Created by Mac on 2/3/23.
//


import UIKit
class TabbarVC:UITabBar{
    private var shapeLaure:CALayer? = nil
    private func addShape(){
        let shapelayre = CAShapeLayer()
        shapelayre.path = cretePayh()
        shapelayre.strokeColor = UIColor.clear.cgColor
        shapelayre.fillColor = UIColor.white.cgColor
        shapelayre.lineWidth = 2.0
        shapelayre.shadowOffset = CGSize(width: 0, height: 0)
        shapelayre.shadowRadius = 10
        shapelayre.shadowColor = UIColor.black.cgColor
        shapelayre.shadowOpacity = 0.3
        
        if let order = self.shapeLaure{
            self.layer.replaceSublayer(order, with: shapelayre)
        }else{
            self.layer.insertSublayer(shapelayre, at: 0)
            
        }
        self.shapeLaure = shapelayre
        
    }
    override func draw(_ rect:CGRect){
        self.addShape()
        self.unselectedItemTintColor = UIColor.gray
        self.tintColor = UIColor.init(cgColor: CGColor(red: 0.143, green: 0.636, blue: 0.937, alpha: 1))
        
    }
    func cretePayh() -> CGPath{
        let heigh:CGFloat = 15
        let path = UIBezierPath()
        let centerwhigh = self.frame.width / 2
        
        path.move(to: CGPoint(x: 0, y: -20))
        path.addQuadCurve(to: CGPoint(x: self.frame.width, y: -20), controlPoint: CGPoint(x: centerwhigh, y: heigh))
        path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))
        path.addLine(to: CGPoint(x: 0, y: self.frame.height))
        path.close()
        return path.cgPath
        
    }

}
