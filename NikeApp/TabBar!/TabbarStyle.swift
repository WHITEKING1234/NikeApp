//
//  TabbarStyle.swift
//  NikeApp
//
//  Created by Mac on 1/3/23.
//

import Foundation
import UIKit
class CustomTabbar:UITabBarController,UITabBarControllerDelegate{
    
    required init?(coder:NSCoder){
        super.init(coder:coder)
    }
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.delegate = self
        self.selectedIndex = 1
        setup()
    }
    func setup(){
        let midleButton = UIButton(frame: CGRect(x: (self.view.bounds.width / 2) - 28  ,y: -30,width: 60,height: 60))
        midleButton.setBackgroundImage(UIImage(systemName: "basketball.fill" ), for: .normal)
        midleButton.layer.shadowColor = UIColor.black.cgColor
        midleButton.layer.shadowOpacity = 0.1
        midleButton.layer.shadowOffset = CGSize(width: 4, height: 4)
        
        self .tabBar.addSubview(midleButton)
        midleButton.addTarget(self, action: #selector(menuButton), for: .touchUpInside)
        self.view.layoutIfNeeded()
        
    }
    @objc func menuButton(){
        self.selectedIndex = 1
    }
    
    
    
}
