//
//  NikeViewController.swift
//  NikeApp
//
//  Created by Mac on 10/3/23.
//

import UIKit
import Kingfisher

class NikeViewController: UIViewController {
    
    
    var dish : Nike!
    
    @IBOutlet weak var ButtoBasket: UIButton!
    
    
    @IBOutlet weak var PriceLable: UILabel!
    
    @IBOutlet weak var NikeImageg: UIImageView!
    
    @IBOutlet weak var NikeLable: UILabel!
    
    @IBOutlet weak var NikeNameLable: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        Nikeapp2()
        ButtoBasket.layer.cornerRadius = 20
        ButtoBasket.clipsToBounds = true
        ButtoBasket.layer.shadowOffset = .zero
        ButtoBasket.layer.shadowOpacity = 1
        ButtoBasket.layer.shadowRadius = 4
        ButtoBasket.layer.shadowColor = UIColor.black.cgColor
        NikeLable.layer.shadowOffset = .zero
        NikeLable.layer.shadowOpacity = 1
        NikeLable.layer.shadowRadius = 4
        NikeLable.layer.shadowColor = UIColor.black.cgColor
        NikeNameLable.layer.shadowOffset = .zero
        NikeNameLable.layer.shadowOpacity = 0.5
        NikeNameLable.layer.shadowRadius = 4
        NikeNameLable.layer.shadowColor = UIColor.black.cgColor
        
        
        
        NikeLable.transform = CGAffineTransformMakeRotation(92.67)
        NikeImageg.transform = CGAffineTransformMakeRotation(44.90)
        
        NikeImageg.layer.shadowOffset = .zero
        NikeImageg.layer.shadowOpacity = 0.5
        NikeImageg.layer.shadowRadius = 4
        NikeImageg.layer.shadowColor = UIColor.black.cgColor
        

    }
    private func Nikeapp2(){
        NikeImageg.kf.setImage(with: URL(string: dish.Image))
        PriceLable.text = dish.price
        NikeNameLable.text = dish.name
        
    }
}

